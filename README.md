# Julia for modeling, simulation, control design and control experiments of the laboratory ball-and-hoop experiment

The system to be controlled is described in the paper Gurtner, Martin, and Jiří Zemánek. “Ball in Double Hoop: Demonstration Model for Numerical Optimal Control.” IFAC-PapersOnLine, 20th IFAC World Congress, 50, no. 1 (July 1, 2017): 2379–84. https://doi.org/10.1016/j.ifacol.2017.08.429.

Some information on design and fabrication of the experimental gadget is at https://aa4cc.github.io/flying-ball-in-hoop/ with sources at https://github.com/aa4cc/flying-ball-in-hoop.
