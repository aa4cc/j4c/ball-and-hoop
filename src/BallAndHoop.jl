module BallAndHoop

using DifferentialEquations
using ModelingToolkit, ModelingToolkitStandardLibrary.Blocks
using ControlSystems
using HDF5
using LibSerialPort

# Write your package code here.
include("model.jl")
include("simulation.jl")
include("serial_wrapper.jl")

export load_hdf5_data
export simulate_system
export simulate_feedback_loop
export ball_and_hoop
export open_serial
export build_frame
export receive_frame
export send_frame

end
