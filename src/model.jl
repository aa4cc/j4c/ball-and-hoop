function load_hdf5_data(file::String, experiment::String)
  fid = h5open(file, "r")
  group = fid[experiment]
  timestamps = read(group["Timestamps"])
  states = read(group["States"])
  input = read(group["Input"])
  return timestamps, states, input
end

"""
    ball_and_hoop(; name, OP = zeros(4))

Creates an ODEProblem with parameter p represented by a 1xN matrix of sampled control input.
Returns the solution to a system of ODEs.
!!! warning
    This function internally uses callbacks, therefore the timestamps, when callback occures, are computed based on length of the input vector and the timespan given by a user using this formula:

    ``T_s = \\frac{\\mathrm{tspan[2]}}{\\mathrm{length(p)-1}}``

    ```@example
    timestamps = collect(tspan[1]:Tₛ:tspan[2])
    ```

    For example given tspan = (0,10) and given matrix p = [0 0.2 0.2 -0.2 -0.1] the timestamps result in [0 2.5 5 7.5 10]

Example of usage:
```@example
x₀ = [0, 0, 0, 0]  # θ, θ̇, ψ, ψ̇
tspan = (0,20)
input = [0 0.5 -0.5 1 -1]
solution = ball_and_hoop_DE_solve(x₀, tspan, input)
```
"""
function ball_and_hoop(; name, OP=zeros(4))
  @variables t
  # Create input and output structures
  @named input = RealInput()
  @named output = RealOutput()

  # Saturation block
  @named saturation = Limiter(y_max=0.7)

  # Define state variables and initialize them to the OP
  @variables theta(t) = OP[1]
  @variables d_theta(t) = OP[2]
  @variables psi(t) = OP[3]
  @variables d_psi(t) = OP[4]

  # Define parameters of the ODEs and initialize them
  @parameters L1 = 586.3694971455833 [tunable = true]
  @parameters J12 = 0.06769766279210704 [tunable = true]
  @parameters J11 = -0.4818896328354544 [tunable = true]
  @parameters K1 = -5.119508012862879 [tunable = true]
  @parameters J22 = -0.33509889971201284 [tunable = true]
  @parameters J21 = -0.0032686776857956444 [tunable = true]
  @parameters L2 = 210.1200001949957 [tunable = true]
  @parameters K2 = -73.63359905056758 [tunable = true]
  dt = Differential(t)

  # Vector of states and parameters
  states = [theta, d_theta, psi, d_psi]
  parameters = [L1, J12, J11, K1, J22, J21, L2, K2]

  # State space equations
  eqs = [
    dt(theta) ~ d_theta
    dt(d_theta) ~ J11 * d_theta + K1 * sin(psi) + J12 * d_psi + L1 * saturation.y
    dt(psi) ~ d_psi
    dt(d_psi) ~ J21 * d_theta + K2 * sin(psi) + J22 * d_psi + L2 * saturation.y
    output.u ~ psi
    saturation.u ~ input.u
  ]

  # Creating ODESystem from equations, independent variable, states and parameters
  sys = ODESystem(eqs, t, states, parameters; name=name)

  # Composing created ODESystem with other ODESystem blocks
  compose(sys, [input, output, saturation])
end

function ball_and_hoop(mb::Real, Ro::Real, Rb::Real, Ib::Real, Ih::Real, bb::Real, bh::Real; name, OP=zeros(4))
  @variables t
  # Create input and output structures
  @named input = RealInput()
  @named output = RealOutput()

  # Saturation block
  @named saturation = Limiter(y_max=0.7)

  # Define state variables and initialize them to the OP
  @variables theta(t) = OP[1]
  @variables d_theta(t) = OP[2]
  @variables psi(t) = OP[3]
  @variables d_psi(t) = OP[4]

  # Second order matrices
  M = [Ib*(Ro/Rb+1)^2+Ih -Ib*Ro/Rb*(Ro/Rb+1); -Ib*Ro/Rb*(Ro/Rb+1) mb*(Ro-Rb)^2+Ib*Ro^2/Rb^2]
  Q = [bb*Ro^2/Rb^2+bh -bb*Ro^2/Rb^2; -bb*Ro^2/Rb^2 bb*Ro^2/Rb^2]
  C = [0; 9.81 * mb * (Ro - Rb)]
  D = [-1; 0]

  try
    J = -M \ Q
    K = -M \ C
    L = -M \ D

    # Define parameters of the ODEs and initialize them
    @parameters L1 = L[1] [tunable = true]
    @parameters J12 = J[1, 2] [tunable = true]
    @parameters J11 = J[1, 1] [tunable = true]
    @parameters K1 = K[1] [tunable = true]
    @parameters J22 = J[2, 2] [tunable = true]
    @parameters J21 = J[2, 1] [tunable = true]
    @parameters L2 = L[2] [tunable = true]
    @parameters K2 = K[2] [tunable = true]
    dt = Differential(t)

    # Vector of states and parameters
    states = [theta, d_theta, psi, d_psi]
    parameters = [L1, J12, J11, K1, J22, J21, L2, K2]

    # State space equations
    eqs = [
      dt(theta) ~ d_theta
      dt(d_theta) ~ J11 * d_theta + K1 * sin(psi) + J12 * d_psi + L1 * saturation.y
      dt(psi) ~ d_psi
      dt(d_psi) ~ J21 * d_theta + K2 * sin(psi) + J22 * d_psi + L2 * saturation.y
      output.u ~ psi
      saturation.u ~ input.u
    ]

    # Creating ODESystem from equations, independent variable, states and parameters
    sys = ODESystem(eqs, t, states, parameters; name=name)

    # Composing created ODESystem with other ODESystem blocks
    compose(sys, [input, output, saturation])

  catch e
    println("Physical parameters lead to singular second order matrices")
    return
  end

end

