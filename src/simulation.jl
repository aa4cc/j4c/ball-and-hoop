function simulate_system(sys::ODESystem, f::Any, init_cond::Vector, tspan::Tuple, solver::Any; kwargs...)
  @named src = TimeVaryingFunction(f)
  connections = [
    sys.input.u ~ src.output.u
  ]
  @named sim_loop = ODESystem(connections, Blocks.t, systems=[src, sys])
  sim_loop = structural_simplify(sim_loop)
  sim_problem = ODEProblem(sim_loop, init_cond, tspan)
  result = solve(sim_problem, solver; kwargs...)
  return result
end

function simulate_feedback_loop(controller::ODESystem, sys::ODESystem, f::Any, init_cond::Vector, tspan::Tuple, solver::Any; kwargs...)
  @named src = TimeVaryingFunction(f)
  connections = [
    controller.input.u ~ -(src.output.u + sys.output.u)
    controller.output.u ~ sys.input.u
  ]
  @named sim_loop = ODESystem(connections, Blocks.t, systems=[src, controller, sys])
  sim_loop = structural_simplify(sim_loop)
  sim_problem = ODEProblem(sim_loop, init_cond, tspan)
  result = solve(sim_problem, solver; kwargs...)
  return result
end
