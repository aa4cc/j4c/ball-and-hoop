using Distributed
@everywhere using PyCall
@everywhere using LibSerialPort
using BaremetalPi

@everywhere function open_serial(serial_name::String, baud_rate::Real; read_timeout_s=0.005)
  sp = SerialPort(serial_name)
  open(sp)
  set_speed(sp, baud_rate)
  set_read_timeout(sp, read_timeout_s)
  flush(sp)
  return sp
end

@everywhere function build_frame(torque::Float32)
  frame = Vector{UInt8}(undef, 8)
  frame[1] = 0xFF
  frame[2] = 0x08
  frame[3] = 0x21
  frame[4:7] = reinterpret(UInt8, [torque]) # already in little endian
  frame[8] = 0x00
  return frame
end

@everywhere function send_frame(port::SerialPort, frame::Vector{UInt8})
  write(port, frame)
end

@everywhere function receive_frame(port::SerialPort)
  frame = Vector{UInt8}(undef, 20)
  set_read_timeout(port, 0.005)

  try
    read!(port, frame)
    clear_read_timeout(port)

    if frame[1] != 0xFF && frame[13] != 0xFF
      return Inf32, Inf32, Inf32
    end

    if frame[2] - 4 != 8 && frame[3] != 0x70
      return Inf32, Inf32, Inf32
    end

    velocity = reinterpret(Float32, frame[4:7])[1]
    position = reinterpret(Float32, frame[8:11])[1]

    if frame[14] - 4 != 4 && frame[15] != 0x71
      return Inf32, Inf32, Inf32
    end

    iq = reinterpret(Float32, frame[16:19])[1]
    return velocity, position, iq

  catch e
    if isa(e, LibSerialPort.Timeout)
      return Inf32, Inf32, Inf32
    else
      rethrow()
    end
  end
end

@everywhere function compute_torque_PID(e_vec::Vector{Float64}, u_vec::Vector{Float64})
  a2 = 1.9895532268941687
  a1 = -3.781042887492248
  a0 = 1.837946360416423
  b2 = 1
  b1 = -1.4475426500133597
  b0 = 0.4475426500133597

  torque = Float32((1 / b2) * (a2 * e_vec[3] + a1 * e_vec[2] + a0 * e_vec[1] - b1 * u_vec[2] - b0 * u_vec[1]))
  if torque >= 0.7
    return Float32(0.7)
  elseif torque <= -0.7
    return Float32(-0.7)
  else
    return torque
  end
end

@everywhere function compute_torque_lead(e_vec::Vector{Float64}, u_vec::Vector{Float64})

  a1 = 2.4342806945451483
  a0 = -1.1514210418177229
  b1 = 1
  b0 = 0.28285965272742575

  torque = Float32((1 / b1) * (a1 * e_vec[2] + a0 * e_vec[1] - b0 * u_vec[1]))
  if torque >= 0.4
    return Float32(0.4)
  elseif torque <= -0.4
    return Float32(-0.4)
  else
    return torque
  end
end

@everywhere function process_fun(chn_1::Any, chn_2::Any, chn_3::Any, id::Int)
  # Chn_x should be type of RemoteChannel
  try
    # UART communication process
    if id == 2
      sp = open_serial("/dev/ttyAMA0", 115200)
      while true
        if isready(chn_1)
          torque = take!(chn_1)
        end
        frame = build_frame(torque)
        send_frame(sp, frame)
        velocity, position, iq = receive_frame(sp)

        put!(chn_2, (velocity, position, iq))
        sleep(0.02)
      end

      # Computation process
    elseif id == 3
      u_vec = zeros(3)
      e_vec = zeros(3)
      ref = 0
      while true
        # Obtain current angle of the ball and compute the difference
        if isready(chn_1)
          e_vec[3] = ref - take!(chn_1)
        end
        # # Compute desired torque
        u_vec[3] = compute_torque(e_vec, u_vec)
        put!(chn_2, u_vec[3])

        if isready(chn_3)
          velocity, position, iq = take!(chn_3)
        end
        # Shift values of u and e for the next computation
        u_vec = push!(u_vec[2:3], 0)
        e_vec = push!(e_vec[2:3], 0)
        sleep(0.02)
      end

      # PiCamera process    
    elseif id == 4
      # Include python library to start camera
      # @everywhere py"""
      # import sys
      # sys.path.insert(0,".")
      # """
      # @everywhere @pyimport sharepos

      shared_position = sharepos.SharedPosition()
      while true
        position = shared_position.read()
        # # Calculating angle in radians
        angle = Float32(360 * (atan(240 - position[1], position[2] - 240)) / 2pi)
        put!(chn_1, angle)
        sleep(0.02)
      end
    end
  catch e
    @show e
    throw(e)
  end
end

function main_multiprocessing()
  button_gpio_num = 6

  # Start processes
  addprocs(3)

  # Create remote channels for communication between processes
  chn23 = RemoteChannel(() -> Channel{Tuple{Float32,Float32,Float32}}(1), 3)
  chn32 = RemoteChannel(() -> Channel{Float32}(1), 2)
  chn43 = RemoteChannel(() -> Channel{Float32}(1), 4)

  init_gpio()
  gpio_set_mode(button_gpio_num, :in)

  # Wait for start button
  println("Press the button")
  while true
    if !gpio_read(button_gpio_num)
      println("Button pressed")
      # Spawn processes with certain channels
      for (pid, ch1, ch2, ch3) in zip(workers(), [chn32, chn43, chn43], [chn23, chn32, chn32], [chn23, chn23, chn23])
        remote_do(process_fun, pid, ch1, ch2, ch3, myid())
      end
      println("Processes spawned")
    end
  end
end

function main_singleprocess()
  button_gpio_num = 6

  sp = open_serial("/dev/ttyAMA0", 115200)

  ref = 0

  init_gpio()
  gpio_set_mode(button_gpio_num, :in)

  # For the PID
  u_vec = zeros(3)
  e_vec = zeros(3)

  # For the lead
  # u_vec = zeros(2)
  # e_vec = zeros(2)


  # Wait for start button
  println("Press the button")
  while true
    if !gpio_read(button_gpio_num)
      println("Button pressed")

      while true
        position = shared_position.read()
        angle = Float32((atan(240 - position[1], position[2] - 240)))
        push!(angles, angle)
        e_vec[3] = ref - angle
        # e_vec[2] = ref - angle
        println("Angle is $angle")
        u_vec[3] = compute_torque_PID(e_vec, u_vec)
        # u_vec[2] = compute_torque_lead(e_vec, u_vec)
        torque = u_vec[3]
        torque = u_vec[2]
        push!(torques, torque)
        println("Torque is $torque")
        frame = build_frame(Float32(u_vec[3]))
        # frame = build_frame(Float32(u_vec[2]))
        send_frame(sp, frame)
        velocity, pos, iq = receive_frame(sp)
        push!(speeds, velocity)
        if velocity == Inf32 || pos == Inf32 || iq == Inf32
          flush(sp)
        end
        # Shift values of u and e for the next computation
        u_vec = push!(u_vec[2:3], 0)
        e_vec = push!(e_vec[2:3], 0)
        # u_vec = [u_vec[2], 0]
        # e_vec = [e_vec[2], 0]
        sleep(0.02)
      end
    end
  end
end

py"""
import sys
sys.path.insert(0,".")
"""
@pyimport sharepos
shared_position = sharepos.SharedPosition()
main_singleprocess()


