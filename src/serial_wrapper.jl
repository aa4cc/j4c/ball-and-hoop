using LibSerialPort

function open_serial(serial_name::String, baud_rate::Real; read_timeout_s=0.005)
  sp = SerialPort(serial_name)
  open(sp)
  set_speed(sp, baud_rate)
  set_read_timeout(sp, read_timeout_s)
  flush(sp)
  return sp
end

function build_frame(torque::Float32)
  frame = Vector{UInt8}(undef, 8)
  frame[1] = 0xFF
  frame[2] = 0x08
  frame[3] = 0x21
  frame[4:7] = reinterpret(UInt8, [torque]) # already in little endian
  frame[8] = 0x00
  return frame
end

function receive_frame(port::SerialPort)
  frame = Vector{UInt8}(undef, 20)
  set_read_timeout(port, 0.005)

  try
    read!(port, frame)
    clear_read_timeout(port)

    if frame[1] != 0xFF && frame[13] != 0xFF
      return Inf32, Inf32, Inf32
    end

    if frame[2] - 4 != 8 && frame[3] != 0x70
      return Inf32, Inf32, Inf32
    end

    velocity = reinterpret(Float32, frame[4:7])[1]
    position = reinterpret(Float32, frame[8:11])[1]

    if frame[14] - 4 != 4 && frame[15] != 0x71
      return Inf32, Inf32, Inf32
    end

    iq = reinterpret(Float32, frame[16:19])[1]
    return velocity, position, iq

  catch e
    if isa(e, LibSerialPort.Timeout)
      return Inf32, Inf32, Inf32
    else
      rethrow()
    end
  end
end

function send_frame(port::SerialPort, frame::Vector{UInt8})
  write(port, frame)
end

