# BallAndHoop Documentation
```@meta
CurrentModule = BallAndHoop
```

# BallAndHoop

Documentation for [BallAndHoop](https://gitlab.fel.cvut.cz/aa4cc/j4c/ball-and-hoop).

```@index
```

```@docs
ball_and_hoop_DE_solve(x₀::Vector{Float64}, tspan::Tuple{Float64,Float64}, p::Matrix{Float64})
ball_and_hoop_DE_solve(x₀::Vector{Float64}, tspan::Tuple{Float64,Float64}, p)
linearize_ball_and_hoop(op::Vector{Float64})
construct_linear_system(op::Vector{Float64})
ball_and_hoop_sys
```
