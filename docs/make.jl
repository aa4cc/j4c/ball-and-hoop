using Pkg
pkg"activate .."

using BallAndHoop
using Documenter

DocMeta.setdocmeta!(BallAndHoop, :DocTestSetup, :(using BallAndHoop); recursive=true)

makedocs(;
  modules=[BallAndHoop],
  authors="Štěpán Ošlejšek",
  #repo="https://gitlab.fel.cvut.cz/oslejste/BallAndHoop.jl/blob/{commit}{path}#{line}",
  repo="https://gitlab.fel.cvut.cz/aa4cc/j4c/ball-and-hoop",
  sitename="BallAndHoop.jl",
  format=Documenter.HTML(;
    prettyurls=get(ENV, "CI", "false") == "true",
    canonical="https://oslejste.gitlab.io/BallAndHoop.jl",
    edit_link="main",
    assets=String[]
  ),
  pages=[
    "Home" => "index.md",
    "Modeling" => "modeling.md",
  ]
)
